# GitLab Docs Archives

The Docs archives site hosts documentation for releases that are no longer included on docs.gitlab.com.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
brew bundle --no-lock
yarn
```

### Compile and Hot-Reload for Development

```sh
yarn run dev
```

### Compile and Minify for Production

```sh
yarn run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
yarn run lint
```

## Parallel deployments

Starting with GitLab 17.8, we're able to use
[parallel deployments](https://new.docs.gitlab.com/user/project/pages/#parallel-deployments)
and not rely on the huge Docker image that would occasionally fail to deploy.

History:

- [Final MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/175563)
  that enabled us to use parallel deployments.
- [Issue](https://gitlab.com/gitlab-org/gitlab-docs-archives/-/issues/25)
  discussing how to implement parallel deployments for the archives. You can
  go through the relevant merge requests to get an idea how we switched from
  the monolithic deployment to parallel deployments.

### Add a new version to the archives

To add a new version to the archives, simply create a branch from `main`, named
after that version. For example:

```shell
# Make sure you're on main and on latest changes
git checkout main
git pull origin main

# Create the branch and push it
git branch 17.0
git push origin 17.0
```

Alternatively, you can [use the UI](https://gitlab.com/gitlab-org/gitlab-docs-archives/-/branches/new).
