#!/usr/bin/env bash
#
# Write a JSON file that lists all the versions on the Archives site.
# We need the list of versions for the version selector on archives.docs.gitlab.com.
#

# Exit on any error
set -e

# Exit if any command in a pipeline fails
set -o pipefail

# Get all branches (both local and remote), filter version pattern, and sort
branches=$(git branch -a | grep -E '^\s*remotes\/origin\/[0-9]+\.[0-9]+$' | sed 's/^\s*//;s/^remotes\/origin\///' | sort -t. -k1,1n -k2,2n | uniq)

# Create JSON array
json="["

first=true
while IFS= read -r branch; do
    if [ "$first" = true ]; then
        first=false
    else
        json="$json,"
    fi
    json="$json\n  \"$branch\""
done <<< "$branches"

json="$json\n]"

# Create the public directory if it doesn't exist
mkdir -p public

# Write to file with proper formatting
echo -e "$json" > public/archive_versions.json
