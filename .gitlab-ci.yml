default:
  image: node:${NODE_VERSION}-alpine
  tags:
    - gitlab-org
  retry: 2

variables:
  DOCKER_VERSION: "26.1.2"
  NODE_VERSION: "22"
  SHELLCHECK_VERSION: "0.10.0"

#
# workflow:rules to prevent duplicate pipelines when pushing to a branch with an open MR.
#
workflow:
  rules:
    # Prevent branch pipelines if an MR is open on the branch.
    - if: $CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "push" && $CI_OPEN_MERGE_REQUESTS
      when: never
    # Allow merge request and scheduled pipelines.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_PIPELINE_SOURCE == "pipeline"'
    - if: '$CI_PIPELINE_SOURCE == "trigger"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$CI_COMMIT_BRANCH =~ /^\d{1,2}\.\d{1,2}$/'

stages:
 - build
 - test
 - deploy

.yarn:
  before_script:
    - node --version
    # https://github.com/nodejs/corepack?tab=readme-ov-file#manual-installs
    - npm uninstall -g yarn pnpm
    - npm install -g corepack@${COREPACK_VERSION}
    - corepack enable
    - yarn --version
    - yarn install --immutable

build_homepage:
  stage: build
  extends:
    - .yarn
  script:
    - yarn run build
  artifacts:
    paths:
      - dist
    expire_in: 1d

shellcheck:
  image: koalaman/shellcheck-alpine:v${SHELLCHECK_VERSION}
  stage: test
  needs: []
  dependencies: []
  script:
    - shellcheck scripts/*

lint:
  stage: test
  needs: []
  dependencies: []
  extends:
    - .yarn
  script:
    - yarn run lint

# Test that scripts/build-version-list.sh works as expected
test_archive_versions_json:
  image: alpine
  stage: test
  needs: []
  dependencies: []
  variables:
    GIT_DEPTH: 0
    GIT_STRATEGY: clone
  script:
    - apk add bash jq git
    - git branch -a
    - mkdir public/
    - echo "Found versions:"
    - scripts/build-version-list.sh
    - cat public/archive_versions.json
    - cat public/archive_versions.json | jq type 1>/dev/null

#
# Download the latest archives image and deploy its contents to Pages
#
pages-production:
  image: alpine
  stage: deploy
  needs:
    - build_homepage
  environment:
    name: production
    url: 'https://archives.docs.gitlab.com'
  variables:
    GIT_DEPTH: 0
  pages:
    path_prefix: ""
  script:
    # Include the static homepage and assets
    - mv dist/ public/
    # Generate a list of versions by parsing archives.Dockerfile
    - apk add bash git
    - scripts/build-version-list.sh
  artifacts:
    paths:
      - public
    expire_in: 1d
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

#
# Download the stable archive image and deploy its contents to Pages using
# parallel deployments,
# https://docs.gitlab.com/ee/user/project/pages/#parallel-deployments
#
pages-archives:
  image: registry.gitlab.com/gitlab-org/gitlab-docs/archives:${CI_COMMIT_REF_NAME}
  stage: deploy
  environment:
    name: archives/${CI_COMMIT_REF_NAME}
    url: https://archives.docs.gitlab.com/${CI_COMMIT_REF_NAME}
  pages:
    path_prefix: ${CI_COMMIT_REF_NAME}
    expire_in: never
  script:
    - du -sh /usr/share/nginx/html
    - mv /usr/share/nginx/html/${CI_COMMIT_REF_NAME}/ public/
    - ls -la public
  artifacts:
    paths:
      - public
    expire_in: 1d
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^15\.[0-5]$/'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^\d{1,2}\.\d{1,2}$/'
      when: always

#
# Extra job to use the old registry directory. Prior to 15.6,
# we use gitlab-docs:<version>.
# We can drop this job after 18.0 is released.
#
pages-archives-pre-15.6:
  image: registry.gitlab.com/gitlab-org/gitlab-docs:${CI_COMMIT_REF_NAME}
  stage: deploy
  environment:
    name: archives/${CI_COMMIT_REF_NAME}
    url: https://archives.docs.gitlab.com/${CI_COMMIT_REF_NAME}
  pages:
    path_prefix: ${CI_COMMIT_REF_NAME}
    expire_in: never
  script:
    - du -sh /usr/share/nginx/html
    - mv /usr/share/nginx/html/${CI_COMMIT_REF_NAME}/ public/
    - ls -la public
  artifacts:
    paths:
      - public
    expire_in: 1d
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^15\.[0-5]$/'
