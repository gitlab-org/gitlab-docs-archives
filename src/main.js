import Vue from "vue";
import App from "./app.vue";

import "./assets/main.scss";

new Vue({
  render: (h) => h(App),
}).$mount("#app");
