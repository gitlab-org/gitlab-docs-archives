import path from "node:path";
import { fileURLToPath } from "node:url";
import { FlatCompat } from "@eslint/eslintrc";
import js from "@eslint/js";
import pluginVue from 'eslint-plugin-vue';

const compat = new FlatCompat({
  baseDirectory: path.dirname(fileURLToPath(import.meta.url)),
  recommendedConfig: js.configs.recommended,
  allConfig: js.configs.all
});

export default [
  ...pluginVue.configs['flat/vue2-recommended'],
  {
    ignores: [
      "**/eslint.config.mjs",
      "**/vite.config.js",
      "**/postcss.config.js",
      "dist/",
      "node_modules/",
    ],
  },
  ...compat.extends("plugin:@gitlab/default"),
  {
    rules: {
      "no-console": ["error", {
        allow: ["info", "warn", "error"],
      }],
    },
  }
];